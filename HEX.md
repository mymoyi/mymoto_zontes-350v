## 解析HEX

#epse32

| model      | 数据来源  |
|---------|----|
| 029E    | air001 |
| 029D    | air780EG |
| 029C    | air103 |


#Air001

| pid      | 描述  |示例 |解析 |
|---------|----|----|----|
|00 - 0F|开关状态 0 或者 1 | 0 |value:toHex()|
|00 - 0F|转向、制动、解锁等 | 1 |具体定义还没有确定|

#Air780
| pid      | 描述  |示例 |解析 |
|---------|----|----|----|
| 50    | 时间戳 |1706325469 | toHex(),16 |
| 51    | 毫秒时间 |179 |tonumber(value:toHex(),16)|
| 52    | VBAT电压| 4098 |tonumber(value:toHex(),16)|
| 60    | NET状态| 0-3  |tonumber(value:toHex(),16)|
| 61    | NET信号RSRP| 40-140/999 |tonumber(value:toHex(),16)|
| 80    | GPS状态| 1/0 |tonumber(value:toHex(),16)|
| 81    | GPS信息| lng,lat...| json |

#Air103
| pid      | 描述  |示例 |解析 |
|---------|----|----|----|
| C0    | 档位信息| 0-6/97/98/99| tonumber(value:toHex(),16) |
