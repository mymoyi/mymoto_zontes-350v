PROJECT = "gnss"
VERSION = "1.0.0"

-- sys库是标配
_G.sys = require("sys")
_G.sysplus = require("sysplus")

--添加硬狗防止程序卡死
if wdt then
    log.info("拥有硬件看门狗A")
    wdt.init(30000) -- 初始化watchdog设置为30s 100
    sys.timerLoopStart(wdt.feed, 10000) -- 10s喂一次狗
end

local uartGps_id = 2 -- uartGps_id air带GPS模块的貌似都在2接口
local uartAir103_id = 1 


local test_pin = 22

local net_led_pin = 27
-- 初始化输出口模块
gpio.setup(net_led_pin, 0)
gpio.setup(test_pin, 0)

libgnss.clear() -- 清空数据,兼初始化
uart.setup(uartGps_id, 115200) -- 启动
uart.setup(uartAir103_id,115200) -- 启动


-- 测试
sys.timerLoopStart(function()
    local a = gpio.get(test_pin);
    if a == 1 then
        a = 0
    else
        a = 1
    end
    gpio.set(test_pin,a)
end,5000)

sys.taskInit(function()
    -- 初始化GPS 打开GPS电源
    log.info("GPS", "start")
    pm.power(pm.GPS, true)
    libgnss.bind(uartGps_id, uart.VUART_0)
    -- SIM初始化
    if mobile then        
        device_id = mobile.imei()
        sys.waitUntil("SIM_IND", 2000)
        if mobile.simPin() == true then
            log.info("sim卡已就绪")
            log.info("开始注册网络")
            sys.waitUntil("IP_READY", 10000)
            if mobile.status() == 1 then
                log.info('网络已就绪')
                gpio.set(net_led_pin, 1) -- 点亮net灯
                socket.sntp() -- 校对时间 
            else
                log.info('网络注册失败')
            end
        else
            log.info('sim',"未检测到sim卡")
        end
        mobile.setAuto(10000,30000, 5) -- SIM暂时脱离后自动恢复，30秒搜索一次周围小区信息
    end
end)




-- 循环发送状态给air103
sys.timerLoopStart(function()
    -- 时间
    local tm = socket.ntptm()        
    if tm.tsec > 1706275235 then
        send_uart_air103_int_data(0x50,tm.tsec)
        send_uart_air103_int_data(0x51,tm.tms)
        -- log.info('tm.tms',tm.tsec,tm.tms)
    end

    -- 电压
    adc.open(adc.CH_VBAT)
    local vbat = adc.get(adc.CH_VBAT)
    adc.close(adc.CH_VBAT)
    -- send_uart_air103_int_data(0x52,vbat)

    -- GPS状态
    local rmc = libgnss.getRmc(2) or {}
    send_uart_air103_data(0x81,json.encode(rmc or {}))

    -- NET状态
    -- send_uart_air103_int_data(0x60,mobile.status())
    -- send_uart_air103_int_data(0x61,mobile.rsrp())

end,1000)

function send_uart_air103_int_data(pid,data)
    local hexString = string.format("%02X", data)
    -- 补位
    if #hexString % 2 == 1 then
        hexString = "0"..hexString
    end
    local decodedText = ""
    for i = 1, #hexString, 2 do
        local byteValue = tonumber(string.sub(hexString, i, i+1), 16)
        decodedText = decodedText .. string.char(byteValue)
    end
    -- log.info("HexString",hexString,hexString:toHex(),data,decodedText:toHex())
    send_uart_air103_data(pid,decodedText)
end

function send_uart_air103_data(pid,data)
    local uart_data =  string.char(0x02, 0x9d, #data, pid)    
    uart_data = uart_data..data
    local dataCheck = calculateChecksum(uart_data)
    local byteValue = tonumber(dataCheck, 16)
    dataCheck = string.char(byteValue)
    uart_data = uart_data..dataCheck.."\r\n"    
    uart.write(uartAir103_id,uart_data)
    -- log.info('uart_aie103传输',pid,data,uart_data:toHex())
end

-- 计算校验位，使用异或运算
function calculateChecksum(data)
    local checksum = 0
    for i = 1, #data do
        checksum = bit.bxor(checksum, string.byte(data, i))
    end
    return string.format("%02X", checksum)
end


-- 订阅sntp对时
sys.subscribe("NTP_UPDATE", function()
    -- log.info("sntp对时", "time", os.date())
    local tm = socket.ntptm()        
    -- log.info("tm数据", json.encode(tm))
    log.info("时间戳", string.format("%u.%03d", tm.tsec, tm.tms))
end)

-- 订阅sntp错误
sys.subscribe("NTP_ERROR", function()
    log.info("socket对时", "sntp error")
    socket.sntp()
end)


-- 订阅GNSS状态编码
sys.subscribe("GNSS_STATE", function(event, ticks)
    -- event取值有 
    -- FIXED 定位成功
    -- LOSE  定位丢失
    -- ticks 是事件发生的时间,一般可以忽略
    if event == "FIXED" then
        -- log.info("gnss", "定位成功", locStr)
        send_uart_air103_int_data(0x80,1)
    elseif event == "LOSE" then
        -- log.info("gnss", "定位丢失")
        send_uart_air103_int_data(0x80,0)

    end
end)

-- 订阅基站数据的查询 模块本身会周期性查询基站信息,但通常不包含临近小区
sys.subscribe("CELL_INFO_UPDATE", function()
    -- log.info("cell", json.encode(mobile.getCellInfo()))
end)


-- 用户代码已结束---------------------------------------------
-- 结尾总是这一句
sys.run()
-- sys.run()之后后面不要加任何语句!!!!!
