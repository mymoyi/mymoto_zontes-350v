PROJECT = 'zentes'
VERSION = '0.0.1'
LOG_LEVEL = log.LOG_SILENT

-- sys库是标配
_G.sys = require("sys")
_G.sysplus = require("sysplus")

----------------------------------------
lampRight_pin = 42-- 32
lampLeft_pin = 40 -- 33
-- 制动灯
lamp_brake_pin = 7

-- 挡位传感器
gear_arr_pin = {11, 10, 9, 8, 29, 28}
-- 挡位状态
gear_status = 99
-- 循环读取间隔
loop_time = 500

for index, pin in ipairs(gear_arr_pin) do
    gpio.setup(pin, nil, gpio.PULLUP) 
end

-- 其他芯片的数据沟通uart id 可以都在一个口上 但是我主打一个隔离 
uartAir001_id = 2
uartAir780_id = 3
uartEsp32_id = 4

local lampRight = gpio.setup(lampRight_pin, 0) 
local lampLeft= gpio.setup(lampLeft_pin, 0) 
local lamp_brake= gpio.setup(lamp_brake_pin, 0) 

--添加硬狗防止程序卡死
if wdt then
    log.info("拥有硬件看门狗A")
    wdt.init(30000) -- 初始化watchdog设置为30s
    sys.timerLoopStart(wdt.feed, 10000) -- 10s喂一次狗
end
----------------------------------------
log.info("开机原因", pm.lastReson())
uart.setup(uartAir001_id)                                 
uart.setup(uartAir780_id)                                 
uart.setup(uartEsp32_id)

local PACKET_LENGTH = 8  -- 假设每个数据包的长度是8
-- 收取数据会触发回调, 这里的"receive" 是固定值
uart.on(uartAir001_id, "receive", function(id, len)
    local s = ""
    repeat
        s = uart.read(id, len)
        if #s > 0  then -- #s 是取字符串的长度
            -- log.info("uart_hex", "receive", id, #s, s:toHex())
            if (len % 8) == 0 then
                for i = 1, #s, PACKET_LENGTH do
                    local dataPacket = s:sub(i, i + PACKET_LENGTH - 1)
                    processPacket(dataPacket)
                end
            else
                -- log.info("uart_hex", "Invalid packet, discarding:", s:toHex(),'--',s:toHex(#startPattern))
                -- 在这里处理无效的数据包，或者丢弃它
            end
        end
        if #s == len then
            break
        end
    until s == ""
end)


uart.on(uartAir780_id, "receive", function(id, len)
    local s = ""
    repeat
        s = uart.read(id, len)
        if #s > 0  then -- #s 是取字符串的长度
            local packet = s
            uart.write(uartEsp32_id,s)
            if len >= 8 then
                local receivedStart = packet:sub(1, 2)
                if receivedStart ==  "\x02\x9e" then
                    -- log.info("uart_air780_hex", "029e数据")
                elseif receivedStart ==  "\x02\x9d" then
                    local model = packet:sub(0, 2);
                    local num = packet:sub(3, 3)
                    local nums = tonumber(num:toHex(),16)
                    if #packet == nums + 7 then
                        local pid = packet:sub(4, 4)
                        local value = packet:sub(5, 4 + nums)
                        local checksum = packet:sub(5+nums , 5+nums)
                        local check = calculateChecksum(packet:sub(0, 4 + nums))
                        if checksum:toHex() == check then
                            uart_handle(model,pid,value)
                        else
                            log.error("检查ERRORRRRR",checksum:toHex(),check)
                        end
                    else
                        -- 这里到存日志！如果两组数据在一起那么可以分开！
                        log.info("uart_air780_hex","数据长度过长！是不是两个组在一起了？",#packet,nums + 7,packet:toHex())
                    end
                end
            else
                log.info("uart_air780_hex", "无效的数据包", s:toHex(),'--',s:toHex(#startPattern))
                -- 在这里处理无效的数据包，或者丢弃它
            end
        end
        if #s == len then
            break
        end
    until s == ""
end)


-- 处理数据
function uart_handle(model,pid,value)
    if model == "\x02\x9e" then
        -- log.info("uart_Air001",pid:toHex(),value:toHex())
    elseif model == "\x02\x9d" then
        if pid == "\x50" then
            -- log.info("当前秒级时间戳",tonumber(value:toHex(),16))
        elseif pid == "\x51" then
            -- log.info("当前毫秒时间戳",tonumber(value:toHex(),16),value:toHex())
        elseif pid == "\x60" then
            -- log.info("当前NET状态",tonumber(value:toHex(),16))
        elseif pid == "\x61" then
            -- log.info("当前NET信号RSRP值",tonumber(value:toHex(),16))
        elseif pid == "\x80" then
            -- log.info("当前GPS状态",tonumber(value:toHex(),16))
        elseif pid == "\x81" then
            local rmc = json.decode(value)
            -- log.info("当前GPS定位",string.format("%.7f", rmc.lng),string.format("%.7f", rmc.lat))
        else
            -- log.info("uart_Unknown",tonumber(value:toHex(),16),pid:toHex(),value:toHex())
        end
    else
        -- log.info("uart_Unknown",pid:toHex(),value:toHex())
    end
end

uart.on(uartEsp32_id, "receive", function(id, len)
    local s = ""
    repeat
        s = uart.read(id, len)
        if #s > 0  then -- #s 是取字符串的长度
            log.info("uart_Esp32_hex", "receive", id, #s, s:toHex(),s)

            -- if (len % 8) == 0 then
            --     for i = 1, #s, PACKET_LENGTH do
            --         local dataPacket = s:sub(i, i + PACKET_LENGTH - 1)
            --         processPacket(dataPacket)
            --     end
            -- else
            --     -- log.info("uart_hex", "Invalid packet, discarding:", s:toHex(),'--',s:toHex(#startPattern))
            --     -- 在这里处理无效的数据包，或者丢弃它
            -- end

        end
        if #s == len then
            break
        end
    until s == ""
end)

local startPattern = "\x02\x9E\x01" -- 每个状态数据头都应该是这个
-- 处理单个数据包的函数
function processPacket(packet)
    -- 在这里处理单个数据包
    local receivedStart = packet:sub(1, #startPattern)
    if receivedStart == startPattern then
        
        local pid = packet:sub(4, 4)
        local status = packet:sub(5, 5)
        local checksum = packet:sub(6, 6)

        local calculatedChecksum = calculateChecksum(packet:sub(0,5))
        -- 检查校验值是否匹配
        if calculatedChecksum == checksum:toHex() then
            -- print("校验通过",calculatedChecksum)
            log.info('Air001','状态',pid:toHex(),status:toHex())
            uart.write(uartEsp32_id,packet) -- 转发给esp32
            uart.write(uartEsp32_id,{0x0D,0x0A}) -- 转发给esp32
        else
            -- log.info('Air001',calculatedChecksum,checksum:toHex())
        end
    elseif receivedStart == "\x02\x9E\xFF" then
        log.info('Air001正在启动')
        -- 2s后请求所有的状态
        sys.timerStart(function()
            queryAir001(0xff);
        end, 2000)
    end
end

-- 计算校验位，使用异或运算
function calculateChecksum(data)
    local checksum = 0
    for i = 1, #data do
        checksum = bit.bxor(checksum, string.byte(data, i))
    end
    return string.format("%02X", checksum)
end



function queryAir001(pid)
    log.info("queryAir001",pid)
    local ct = string.char(0xfa,pid,0x01)
    uart.write(uartAir001_id, ct)
end

function queryResult(pid)
    -- 这里添加处理查询结果的逻辑
    print("Query result for PID " .. pid)
end


sys.taskInit(function()
    while 1 do
        lampRight(0)
        lampLeft(1)

        -- -- 打开adc通道2,并读取
        -- if adc.open(3) then
        --     -- 这里使用的是adc.read会返回2个值, 推荐走adc.get函数,直接取实际值
        --     log.info("adc_read", adc.read(3))
        --     log.info("adc_get", adc.get(3))
        -- end
        -- adc.close(2)

        sys.wait(500)        
        lampRight(1)
        lampLeft(0)
        -- uart.write(uartEsp32_id,{0xAA,0x98,0x97,0x96,0x95,0x94,0x93,0x92,0x91,0x90}) -- 转发给esp32
        -- uart.write(uartEsp32_id,{0x0D,0x0A}) -- 转发给esp32
        sys.wait(500)

        -- uart.write(uartEsp32_id,{0x99,0x98,0x97,0x96,0x95,0x94,0x93,0x92,0x91,0x90}) -- 转发给esp32
        -- uart.write(uartEsp32_id,{0x0D,0x0A}) -- 转发给esp32        

    end
end)


-- loop time
sys.timerLoopStart(function()

    local gear = {}
    local gear_i =  0 
    for k, v in pairs(gear_arr_pin) do

        -- log.info('gear_status',k,gpio.get(v))
        if gpio.get(v) == gpio.LOW then
            table.insert(gear, k)
        end
    end
    if #gear == 1 then
        gear_i = gear[1]
    elseif #gear == 0 then
        gear_i = 0
    elseif  #gear < #gear_arr_pin then
        gear_i = 97
    elseif  #gear == #gear_arr_pin then
        gear_i = 98
    else
        gear_i = 99
    end
    
    if gear_i ~= gear_status then
        gear_status = gear_i
    end 
    
    log.info('gear_status',gear_status,#gear,gear_i,#gear_arr_pin)

    send_uart_esp32_int_data(0xC0,gear_status)



end,loop_time)



function send_uart_esp32_data(pid,data)
    local uart_data =  string.char(0x02, 0x9d, #data, pid)    
    uart_data = uart_data..data
    local dataCheck = calculateChecksum(uart_data)
    local byteValue = tonumber(dataCheck, 16)
    dataCheck = string.char(byteValue)
    uart_data = uart_data..dataCheck.."\r\n"    
    uart.write(uartEsp32_id,uart_data)
end

function send_uart_esp32_int_data(pid,data)
    local hexString = string.format("%02X", data)
    -- 补位
    if #hexString % 2 == 1 then
        hexString = "0"..hexString
    end
    local decodedText = ""
    for i = 1, #hexString, 2 do
        local byteValue = tonumber(string.sub(hexString, i, i+1), 16)
        decodedText = decodedText .. string.char(byteValue)
    end
    send_uart_esp32_data(pid,decodedText)
end


-- local leds = ws2812.create(ws2812.PWM, 144, 22)


-- sys.timerLoopStart(function()
--     ws2812.set(leds, 0, 0xFF, 0xAA, 0x11)
--     sys.wait(500)
--     ws2812.set(leds, 5, 0xFFAA11)
-- end,1500)



-- 用户代码已结束---------------------------------------------
-- 结尾总是这一句
sys.run()
-- sys.run()之后后面不要加任何语句!!!!!