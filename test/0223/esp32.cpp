#include "Arduino.h"
#include "TFT_eSPI.h"
#include "RTOS.h"
#include "chrono"
#include "ArduinoJson.h"

const uint16_t screenWidth = 240;
const uint16_t screenHeight = 320;
#define OLED_BL 25

TFT_eSPI tft = TFT_eSPI(screenWidth, screenHeight); /* TFT instance */

struct tm timeinfo{};
time_t air780_time_stamp;
time_t system_time_stamp = 1577808000;
time_t start_time_stamp;
int air780_time_tms;
char formattedTime[32];  // 用于存储格式化后的时间字符串
char formattedTime_s[32];  // 用于存储格式化后的时间字符串
char formattedTime_start[32];  // 用于存储格式化后的时间字符串

char lng[32];
char lat[32];
char test[32];
char memory[32];
char variation[32];
char speed[32];
char gear[16];

String air001[13] = {
        "OFF","OFF","OFF","OFF","OFF","OFF","OFF","OFF","OFF","OFF","OFF","OFF","OFF"
};

JsonDocument doc;
// 获取可用内存的函数
void print_chip_info() {
    uint32_t total_heap_size = heap_caps_get_total_size(MALLOC_CAP_8BIT);
    uint32_t free_heap_size = heap_caps_get_free_size(MALLOC_CAP_8BIT);
    uint32_t used_heap_size = total_heap_size - free_heap_size;
    snprintf(memory, 32, "Memory:%d KB/%d KB",
             spi_flash_get_chip_size() /1024,
             used_heap_size / 1024,free_heap_size  / 1024);
    tft.drawString(memory,30,140);

}


[[noreturn]] void tmsFunction(__attribute__((unused)) void *pvParameters) {
    while (true) {
        air780_time_tms = air780_time_tms + 10;
        if (air780_time_tms >= 1000) {
            air780_time_stamp ++;
            system_time_stamp ++;
            air780_time_tms = air780_time_tms % 1000;
            localtime_r(&air780_time_stamp, &timeinfo);

            snprintf(formattedTime, sizeof(formattedTime), "%04d-%02d-%02d %02d:%02d:%02d:%03d",
                     timeinfo.tm_year + 1900,timeinfo.tm_mon + 1,timeinfo.tm_mday,timeinfo.tm_hour,timeinfo.tm_min,timeinfo.tm_sec,air780_time_tms);
            tft.drawString(formattedTime,30,0);

            localtime_r(&system_time_stamp, &timeinfo);
            snprintf(formattedTime_s, sizeof(formattedTime_s), "%02d:%02d:%02d:%03d",timeinfo.tm_hour,timeinfo.tm_min,timeinfo.tm_sec,air780_time_tms);
            tft.drawString(formattedTime_s,30,12);

            localtime_r(&start_time_stamp, &timeinfo);
            snprintf(formattedTime_start, sizeof(formattedTime_start), "start:%02d:%02d:%02d",timeinfo.tm_hour,timeinfo.tm_min,timeinfo.tm_sec);
            tft.drawString(formattedTime_start,130,12);

        }

        tft.drawString(lng,30,24);
        tft.drawString(lat,30,36);
        tft.drawString(variation,30,48);
        tft.drawString(speed,30,60);


//        tft.drawString("　　　　　　　　　",30,126);
        tft.drawString(gear,30,126);

        
        for (int i = 0; i < 13; i++) {
            tft.drawString(String(air001[i]),0,i * 12 + 12);
        }


        tft.drawString(test,30,128);
        print_chip_info();
        vTaskDelay(pdMS_TO_TICKS(10));  // 0.1秒的延迟
    }
}

long string_to_int(String value){
    String x = "";
    String z = "";
    char y[255];
    for (size_t i = 0; i < value.length(); i++) {
        x = String(value[i], HEX);
        if (String(value[i], HEX).length()%2 == 1){
            x = "0" + x;
        }
        z = z + x;
    }
    z.toCharArray(y, sizeof(y));
    return strtol(y, nullptr, 16);
}

void uart_handle(byte model,byte pid,const String& value){
    String charString;
    String valueString;
    int charInt = 0;
    double json_double_value;

    DeserializationError error;
    switch (pid) {
        case 0x50:
            air780_time_stamp  = string_to_int(value);

            if(start_time_stamp == 0){
                start_time_stamp = air780_time_stamp;
            }

            break;
        case 0x51:
            air780_time_tms = string_to_int(value);
            break;
        case 0x81:
            error = deserializeJson(doc, value);
            if (error) {
                Serial.print(F("deserializeJson() failed: "));
                Serial.println(error.f_str());
                return;
            }else{
                json_double_value = doc["lng"];
                snprintf(lng, sizeof(lng), "lng:%.7f",json_double_value);
                json_double_value = doc["lat"];
                snprintf(lat, sizeof(lat), "lat:%.7f",json_double_value);
                json_double_value = doc["variation"];
                snprintf(variation, sizeof(variation), "variation:%.2f",json_double_value);
                json_double_value = doc["speed"];
                snprintf(speed, sizeof(speed), "speed:%.2f",json_double_value);
            }
            break;
        case 0xC0:
            charInt = string_to_int(value);
            switch (charInt) {
                case 97:
                    charString = "-";
                    break;
                case 98:
                    charString = "Not Started";
                    break;
                case 99:
                    charString = "ERROR";
                    break;
                default:
                    charString = charInt;
                    break;
            }
            snprintf(gear, sizeof(gear), "gear:%s　　　　　　　　",charString.c_str());
            Serial.print("档位数据");
            break;
        default:
            if(model  == 0x9e){
                charString = String(pid, HEX);
                valueString = String(value[0], HEX);
                charInt =  strtol( charString.c_str(), nullptr, 16);
                if(valueString == "1"){
                    air001[charInt] = "ON  ";
                }else{
                    air001[charInt] = "OFF";
                }

            }else{
                
                
                Serial.print("其他数据");
            }
            break;
    }
}

void setup() {
    Serial.begin(115200);
    Serial2.begin(115200);
    setenv("TZ", "CST-8", 1); // 设置中国时区
    tft.init();
    tft.setSwapBytes(true);
    tft.fillScreen(TFT_BLACK); // 填充黑色背景
    pinMode(OLED_BL, OUTPUT); // 设置成输出模式
    analogWrite(OLED_BL, 180); // 0-255，调节亮度
    // 校对过后的触摸参数
    uint16_t calData[5] = {211, 3578, 449, 3476, 6};
    tft.setTouch(calData);
    tft.drawString("MoYi",0,0);

    xTaskCreate(tmsFunction, "tmsTask", 4096, NULL, 5, NULL);

    air780_time_stamp = 1218154088; // 默认 08 年 ～ 哈哈

}

String lastData = ""; // 用于保存上一次读取的未结束的数据

byte calculateChecksum(String data, unsigned int dataSize);


void loop() {
    if (Serial2.available()) {
        String receivedData = Serial2.readStringUntil('\n');
        if (receivedData.length() > 2 ) {
            byte lastByte = receivedData[receivedData.length() - 1];
            byte checkByte = receivedData[receivedData.length() - 2];

            if(lastByte != 0x0d ){
                lastData = lastData + receivedData + '\n';
            }else{
                // 如果 lastData 不为空，则拼接到 receivedData 的前面
                if (!lastData.isEmpty() ) {
                    receivedData = lastData + receivedData;
                    lastData = ""; // 清空 lastData
                }
                int num = receivedData[2];
                if((num+6) == receivedData.length()){
                    String value;
                    byte model = receivedData[1];
                    byte pid = receivedData[3];
                    byte checksum = calculateChecksum(receivedData,receivedData.length() - 2);
                    if(checkByte == checksum){
                        for (int i = 0; i < num; i++) {
                            value = value + receivedData[4 + i];
                        }
                        uart_handle(model,pid,value);
                    }
                }else if((num+6) < receivedData.length()){
                    lastData = ""; // 清空 lastData
                    Serial.print("Received Error！");
                }

                // 打印十六进制数据0x02,0x9e, 0x01
//                Serial.print("Received (Hex): ");
//                for (size_t i = 0; i < receivedData.length(); i++) {
//                    Serial.print(String(receivedData[i], HEX));
//                    Serial.print(' ');
//                }
//                Serial.println(' ');
            }
        }
    }
}

byte calculateChecksum(String data, unsigned int dataSize) {
    // 计算校验位，这里简单地使用异或运算
    byte checksum = 0;
    for (int i = 0; i < dataSize; i++) {
        checksum ^= data[i];
    }
    return checksum;
}

