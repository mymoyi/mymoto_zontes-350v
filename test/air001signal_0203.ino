//                      RX    TX
HardwareSerial Serial2(PA1, PA0);

const int signal_io_num = 15;  // 信号数量IO

// 定义多维数组，每一行包含 PIN、描述和当前状态
// 本来想定义 后来一想反正就是采集数据 那么直接按照顺序返回得了 ～
int signal_io_arr[signal_io_num][3] = {
  { PB6, '左信号灯', 0},  // {PIN, 描述, 当前状态}
  { PB3, '右信号灯', 0},
  { PF0, '其他PF0', 0},
  { PF1, '其他PF1', 0},
  { PB0, '其他1', 0},
  { PA4, '其他2', 0},
  { PB1, '其他3', 0},
  { PA6, '其他4', 0},  
  { PA7, '其他5', 0},
  { PA5, '其他6', 0},
  { PB2, '其他10', 0},
  { PA2, '其他11', 0},
  { PA3, '其他10', 0},
  { PA14, '其他9', 0},
  { PA13, '其他8', 0},
};


void setup() {

  Serial.begin(115200);
  Serial2.begin(115200);
  Serial.printf("\nHello, Air001. \n");


  // 设置每个 IO 口为输入或输出，根据你的需求
  for (int i = 0; i < signal_io_num; i++) {
    if (i == 2) {
      pinMode(signal_io_arr[i][0], INPUT_PULLUP);
    }else{
      pinMode(signal_io_arr[i][0], INPUT);
    }
  }
  sendStartAir();
}

void loop() {

 if (Serial2.available()) {
    String receivedData = Serial2.readStringUntil('\r\n');

    if (receivedData.length() > 0 and (receivedData.length() % 3) == 0) {
      // 打印接收到的字符串
      if (receivedData[0] == 0xfa){
        byte pid = receivedData[1];
        byte mode = receivedData[2];
        if(pid == 0xff and mode == 0x01){
          Serial.println("查询所有状态");
          // 遍历二维数组
          for (int i = 0; i < signal_io_num; i++) {
            sendSignalIo(i);
            Serial.print(i);
          }
        }else if (mode == 0x01){
          Serial.println("查询单独状态");
          int pid_num = (int)pid;
          sendSignalIo(pid_num);
          Serial.print(pid_num);
        }
      }
      // 打印十六进制数据
      Serial.print("Received (Hex): ");
      for (size_t i = 0; i < receivedData.length(); i++) {
        Serial.print(String(receivedData[i], HEX));
        Serial.print(' ');
      }
    }
  }
  // 读取每个 IO 口的状态，每隔 50 毫秒执行一次
  static unsigned long lastTime = 0;
  unsigned long currentTime = millis();
  if (currentTime - lastTime >= 50) {
    lastTime = currentTime;

    for (int i = 0; i < signal_io_num; i++) {
      int pin = signal_io_arr[i][0];
      int prevState = signal_io_arr[i][2];
      int currentState = digitalRead(pin);
      // 如果状态发生改变，输出提醒
      if (currentState != prevState) {
        // 更新当前状态
        signal_io_arr[i][2] = currentState;
        sendSignalIo(i);
      }
    }
  }
  delay(50);
}

void sendSignalIo(int i){
    byte data[] = {0x02,0x9e, 0x01,(byte)i,signal_io_arr[i][2]}; // 数据长度固定为3个字节
    int dataSize = sizeof(data) / sizeof(data[0]);
    sendHexWithChecksum(data, dataSize);
}

void sendStartAir(){
    byte data[] = {0x02,0x9e, 0xff,0xff,0xff}; 
    int dataSize = sizeof(data) / sizeof(data[0]);
    sendHexWithChecksum(data, dataSize);
}

void sendHexWithChecksum(byte* data, int dataSize) {
  // 计算校验位
  byte checksum = calculateChecksum(data, dataSize);
  // 发送数据
  for (int i = 0; i < dataSize; i++) {
    Serial2.write(data[i]);
  }
  Serial2.write(checksum);
  Serial2.println();
}

byte calculateChecksum(byte* data, int dataSize) {
  // 计算校验位，这里简单地使用异或运算
  byte checksum = 0;
  for (int i = 0; i < dataSize; i++) {
    checksum ^= data[i];
  }
  return checksum;
}