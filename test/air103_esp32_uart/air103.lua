PROJECT = 'zentes'
VERSION = '0.0.1'
LOG_LEVEL = log.LOG_SILENT

-- sys库是标配
_G.sys = require("sys")
_G.sysplus = require("sysplus")

----------------------------------------
lampRight_pin = 42
lampLeft_pin = 40


-- 其他芯片的数据沟通uart id 可以都在一个口上 但是我主打一个隔离 
uartAir001_id = 2;
uartAir780_id = 3;
uartEsp32_id = 4;

local lampRight = gpio.setup(lampRight_pin, 0) 
local lampLeft= gpio.setup(lampLeft_pin, 0) 

--添加硬狗防止程序卡死
if wdt then
    log.info("拥有硬件看门狗A")
    wdt.init(30000) -- 初始化watchdog设置为30s
    sys.timerLoopStart(wdt.feed, 10000) -- 10s喂一次狗
end
----------------------------------------
log.info("开机原因", pm.lastReson())

uart.setup(uartAir001_id)                                 
uart.setup(uartAir780_id)                                 
uart.setup(uartEsp32_id)

local PACKET_LENGTH = 8  -- 假设每个数据包的长度是8
-- 收取数据会触发回调, 这里的"receive" 是固定值
uart.on(uartAir001_id, "receive", function(id, len)
    local s = ""

    repeat
        s = uart.read(id, len)
        if #s > 0  then -- #s 是取字符串的长度
            -- log.info("uart_hex", "receive", id, #s, s:toHex())
            if (len % 8) == 0 then
                for i = 1, #s, PACKET_LENGTH do
                    local dataPacket = s:sub(i, i + PACKET_LENGTH - 1)
                    processPacket(dataPacket)
                end
            else
                -- log.info("uart_hex", "Invalid packet, discarding:", s:toHex(),'--',s:toHex(#startPattern))
                -- 在这里处理无效的数据包，或者丢弃它
            end
        end
        if #s == len then
            break
        end
    until s == ""
end)



uart.on(uartAir780_id, "receive", function(id, len)
    local s = ""

    repeat
        s = uart.read(id, len)
        if #s > 0  then -- #s 是取字符串的长度
            log.info("uart_air780_hex", "receive", id, #s, s:toHex(),s)

            -- if (len % 8) == 0 then
            --     for i = 1, #s, PACKET_LENGTH do
            --         local dataPacket = s:sub(i, i + PACKET_LENGTH - 1)
            --         processPacket(dataPacket)
            --     end
            -- else
            --     -- log.info("uart_hex", "Invalid packet, discarding:", s:toHex(),'--',s:toHex(#startPattern))
            --     -- 在这里处理无效的数据包，或者丢弃它
            -- end
        end
        if #s == len then
            break
        end
    until s == ""
end)




uart.on(uartEsp32_id, "receive", function(id, len)
    local s = ""
    repeat
        s = uart.read(id, len)
        if #s > 0  then -- #s 是取字符串的长度
            log.info("uart_Esp32_hex", "receive", id, #s, s:toHex(),s)

            -- if (len % 8) == 0 then
            --     for i = 1, #s, PACKET_LENGTH do
            --         local dataPacket = s:sub(i, i + PACKET_LENGTH - 1)
            --         processPacket(dataPacket)
            --     end
            -- else
            --     -- log.info("uart_hex", "Invalid packet, discarding:", s:toHex(),'--',s:toHex(#startPattern))
            --     -- 在这里处理无效的数据包，或者丢弃它
            -- end
        end
        if #s == len then
            break
        end
    until s == ""
end)

local startPattern = "\x02\x9E\x01" -- 每个状态数据头都应该是这个
-- 处理单个数据包的函数
function processPacket(packet)
    -- 在这里处理单个数据包
    local receivedStart = packet:sub(1, #startPattern)
    if receivedStart == startPattern then
        
        local pid = packet:sub(4, 4)
        local status = packet:sub(5, 5)
        local checksum = packet:sub(6, 6)

        local calculatedChecksum = calculateChecksum(packet:sub(0,5))
        -- 检查校验值是否匹配
        if calculatedChecksum == checksum:toHex() then
            -- print("校验通过",calculatedChecksum)
            log.info('Air001','状态',pid:toHex(),status:toHex())

            uart.write(uartEsp32_id,packet) -- 转发给esp32
            uart.write(uartEsp32_id,{0x0D,0x0A}) -- 转发给esp32

        else
            -- log.info('Air001',calculatedChecksum,checksum:toHex())
        end
    elseif receivedStart == "\x02\x9E\xFF" then
        log.info('Air001正在启动')
        -- 2s后请求所有的状态
        sys.timerStart(function()
            queryAir001(0xff);
        end, 2000)
    end
end

-- 计算校验位，使用异或运算
function calculateChecksum(data)
    local checksum = 0
    for i = 1, #data do
        checksum = bit.bxor(checksum, string.byte(data, i))
    end
    return string.format("%X", checksum)
end



function queryAir001(pid)
    log.info("queryAir001",pid)
    local ct = string.char(0xfa,pid,0x01)
    uart.write(uartAir001_id, ct)
end

function queryResult(pid)
    -- 这里添加处理查询结果的逻辑
    print("Query result for PID " .. pid)
end


sys.taskInit(function()
    while 1 do
        lampRight(0)
        lampLeft(1)
        sys.wait(2000)        
        lampRight(1)
        lampLeft(0)
        uart.write(uartEsp32_id,{0xAA,0x98,0x97,0x96,0x95,0x94,0x93,0x92,0x91,0x90}) -- 转发给esp32
        uart.write(uartEsp32_id,{0x0D,0x0A}) -- 转发给esp32
        sys.wait(2000)

        uart.write(uartEsp32_id,{0x99,0x98,0x97,0x96,0x95,0x94,0x93,0x92,0x91,0x90}) -- 转发给esp32
        uart.write(uartEsp32_id,{0x0D,0x0A}) -- 转发给esp32        


    end
end)


-- 用户代码已结束---------------------------------------------
-- 结尾总是这一句
sys.run()
-- sys.run()之后后面不要加任何语句!!!!!