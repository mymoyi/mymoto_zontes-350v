#include "Arduino.h"
#include "TFT_eSPI.h"


const uint16_t screenWidth = 240;
const uint16_t screenHeight = 320;
#define OLED_BL 25

// 对应的其他代码参考之前的b站esp32 驱动文章
TFT_eSPI tft = TFT_eSPI(screenWidth, screenHeight); /* TFT instance */

void setup() {
    Serial.begin(115200);
    Serial2.begin(115200);

    setenv("TZ", "CST-8", 1); // 设置中国时区
    tft.init();
    tft.setSwapBytes(true);
    tft.fillScreen(TFT_BLACK); // 填充黑色背景
    pinMode(OLED_BL, OUTPUT); // 设置成输出模式
    analogWrite(OLED_BL, 180); // 0-255，调节亮度
    // 校对过后的触摸参数
    uint16_t calData[5] = {211, 3578, 449, 3476, 6};
    tft.setTouch(calData);
    tft.drawString("Test",0,0);
}

int t1 = 1;
String lastData = ""; // 用于保存上一次读取的未结束的数据

byte calculateChecksum(String data, unsigned int dataSize);

void loop() {

    if (Serial2.available()) {
        String receivedData = Serial2.readStringUntil('\n');
        if (receivedData.length() > 0 ) {
            byte lastByte = receivedData[receivedData.length() - 1];
            byte checkByte = receivedData[receivedData.length() - 2];

            if(lastByte != 0x0d ){
                lastData = lastData + receivedData + '\n';
            }else{
                // 如果 lastData 不为空，则拼接到 receivedData 的前面
                if (!lastData.isEmpty()) {
                    receivedData = lastData + receivedData;
                    lastData = ""; // 清空 lastData
                }
                if (receivedData[0] == 0x02 and receivedData[1] == 0x9e){
                    byte checksum = calculateChecksum(receivedData,receivedData.length() - 2);

                    if(checkByte == checksum){
                        int num = receivedData[2];
                        byte pid = receivedData[3];
                        Serial.print("PID:");
                        Serial.print(String(pid, HEX));
                        int value = 0;
                        if (num > 0) {
                            for (int i = 0; i < num; i++) {
                                value += receivedData[4 + i];
                            }
                            tft.drawString(String(pid, HEX) + ": " +  String(value, HEX) , 0,String(pid).toInt() * 10);
                        }
                    }else{
                        Serial.print("Check Error！");
                        Serial.println(lastByte);
                        Serial.println(checkByte);
                        Serial.println(checksum);
                        Serial.println(" Error! ");
                    }
                }
                // 打印十六进制数据0x02,0x9e, 0x01
                Serial.print("Received (Hex): ");
                for (size_t i = 0; i < receivedData.length(); i++) {
                    Serial.print(String(receivedData[i], HEX));
                    Serial.print(' ');
                }
                Serial.println(' ');
            }
        }
    }
}



byte calculateChecksum(String data, unsigned int dataSize) {
    // 计算校验位，这里简单地使用异或运算
    byte checksum = 0;
    for (int i = 0; i < dataSize; i++) {
        checksum ^= data[i];
    }
    return checksum;
}

