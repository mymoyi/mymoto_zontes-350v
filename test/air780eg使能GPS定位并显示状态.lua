PROJECT = "gnss"
VERSION = "1.0.0"

-- sys库是标配
_G.sys = require("sys")
_G.sysplus = require("sysplus")

--添加硬狗防止程序卡死
if wdt then
    log.info("拥有硬件看门狗A")
    wdt.init(30000) -- 初始化watchdog设置为30s
    sys.timerLoopStart(wdt.feed, 10000) -- 10s喂一次狗
end

local gps_uart_id = 2 -- GPS_uart_id air带GPS模块的貌似都在2接口

-- 红/绿/蓝, 各8bit
local red = lcd.rgb565(0xFF, 0x00, 0x00)
local green = lcd.rgb565(0x00, 0xFF, 0x00)
local blue = lcd.rgb565(0x00, 0x00, 0xFF)

libgnss.clear() -- 清空数据,兼初始化
uart.setup(gps_uart_id, 115200) -- 启动

-- 设置初始化屏幕
local spi_id,pin_reset,pin_dc,pin_cs,bl = 0,1,10,8,22 
spi_lcd = spi.deviceSetup(spi_id,pin_cs,0,0,8,20*1000*1000,spi.MSB,1,0)
lcd.init("st7735",{port = "device",pin_dc = pin_dc, pin_pwr = bl, pin_rst = pin_reset,direction = 0,w = 128,h = 160,xoffset = 0,yoffset = 0},spi_lcd)

local param = {
    GPSms = 1000
}
local show_arr = {
    GPS = false,
    NET = false,
    VCC = false,
    LEFT = false,
    RIGHT = false,
    GPSPM = false,
}

local show_arrs = {
    { key = "VCC" , desc = "VCC" ,value = show_arr.VCC},
    { key = "GPSPM" ,desc = "GPSpm" ,value = show_arr.GPSPM},
    { key = "GPS" , desc = "GPS" , value = show_arr.GPS},
    { key = "NET" , desc = "NET" ,value = show_arr.NET},
    { key = "LEFT" ,desc = "Left" ,value = show_arr.LEFT},
    { key = "RIGHT" ,desc = "Right" ,value = show_arr.RIGHT},
    
}


sys.taskInit(function()

    -- 初始化GPS 打开GPS电源

    log.info("GPS", "start")
    pm.power(pm.GPS, true)
    libgnss.bind(gps_uart_id, uart.VUART_0)

    -- SIM初始化
    if mobile then        
        device_id = mobile.imei()
        sys.waitUntil("SIM_IND", 2000)
        if mobile.simPin() == true then
            log.info("sim卡已就绪")
            log.info("开始注册网络")
            sys.waitUntil("IP_READY", 10000)
            if mobile.status() == 1 then
                log.info('网络已就绪')
                show_arr.NET = true
            else
                log.info('网络注册失败')
            end
        else
            log.info('sim',"未检测到sim卡")
        end
    end

end)

-- 初始化lcd的buff缓冲区, 可理解为FrameBuffer区域.
lcd.setupBuff()


sys.timerLoopStart(function()
    
end,1000)



-- 循环开始获取定位
sys.timerLoopStart(function()
    -- log.info('start GPS')
    
    local rmc = libgnss.getRmc(2) or {}
        lcd.clear(0xffffff)
        lcd.setColor(0xffffff,red)
        for i, v in pairs(show_arrs) do      
            local colors = red
            if show_arr[v.key] == true then
                colors = green 
            end
            autoDrawText(i-1,v.desc,colors)   
        end

        adc.open(adc.CH_VBAT)
        local vbat = adc.get(adc.CH_VBAT)
        adc.close(adc.CH_VBAT)
        log.info('vbat',vbat)
        lcd.drawStr(60,45,"vbat:"..vbat)
        local x = 45
        for k, v in pairs(rmc) do
            local val = tostring(v);
            lcd.fill(2,12,x,12,green)
            lcd.drawStr(2,x,k..":"..val)
            x = x + 12
        end
end, param.GPSms)

function autoDrawText(i,text,color)
    local x = 2 
    local y = 10 + math.floor(i / 3) * 20
    local h = i % 3
    if h == 0 then
        x = 2
    elseif h == 1 then
        x = 45
    else
        x = 90
    end
    -- log.info('auto',i,h,x, y, text,color)
    drawTextWithAutoIcon(x, y, text,color)
end
-- 自动化填充文本的函数
function drawTextWithAutoIcon(x, y, text,color)
    lcd.setColor(0xffffff,color)

    lcd.drawStr(x, y, text)

    -- 硬编码图标数据
    local iconData = string.char(
        0x00,0x00,0x60,0x00,0xf0,0x00,0xf8,0x01,0xfc,0x03,0xf6,0x06,0xf6,0x06,0x0c,0x03,0xf8,0x01,0xf0,0x00,0x60,0x00,0x00,0x00
    )
    -- 计算文本宽度，假设一个字符宽度为 8 像素
    local textWidth = #text * 8
    if textWidth>30 then
        textWidth = 32
    end
    -- 绘制图标
    lcd.drawXbm(x + textWidth , y - 10, 12, 12, iconData)
    -- 绘制文本
end


-- sys.timerLoopStart(function()
    
-- end,500)


-- 订阅GNSS状态编码
sys.subscribe("GNSS_STATE", function(event, ticks)
    -- event取值有 
    -- FIXED 定位成功
    -- LOSE  定位丢失
    -- ticks是事件发生的时间,一般可以忽略
    log.info("gnss", "state", event, ticks)
    if event == "FIXED" then
        show_arr.GPS = true
        log.info("gnss", "定位成功", locStr)
        -- pm.power(pm.GPS, false)

    elseif event == "LOSE" then
        log.info("gnss", "定位丢失")
        show_arr.GPS = false
    end
end)

-- 用户代码已结束---------------------------------------------
-- 结尾总是这一句
sys.run()
-- sys.run()之后后面不要加任何语句!!!!!
